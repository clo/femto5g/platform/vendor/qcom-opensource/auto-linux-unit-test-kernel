/* Copyright (c) 2019, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <linux/clk.h>
#include <linux/clk/qcom.h>
#include <linux/platform_device.h>
#include <linux/module.h>
#include <linux/of_device.h>
#include <linux/kernel.h>
#include <linux/reset.h>
#include <linux/kprobes.h>
#include <asm/ptrace.h>
#include <linux/ktf.h>

KTF_INIT();

#define DRV_NAME	"virt_clk_fault_inject_test"

static struct device *dev_fi;
static struct clk *clk_fi;
static struct reset_control *rst_fi;

TEST(fault_injection_1, Inject_clk_get)
{
	tlog(T_INFO, "injecting error in virtio clock driver...\n");
	clk_fi = clk_get(dev_fi, "unit_test_clk_1");
	tlog(T_INFO, "Clock unit_test_clk_1 get return 0x%x\n", clk_fi);
	if (!IS_ERR_OR_NULL(clk_fi))
		ASSERT_FALSE_GOTO(true, err);
	EXPECT_TRUE(true);
	clk_put(clk_fi);

	return;

err:
	terr("fault injection for clk_get failed\n");
}

TEST(fault_injection_2, Inject_clk_prepare)
{
	int ret = 0;

	tlog(T_INFO, "injecting error in virtio clock driver...\n");

	clk_fi = clk_get(dev_fi, "unit_test_clk_1");
	if (!IS_ERR_OR_NULL(clk_fi)) {
		ret = clk_prepare(clk_fi);
		tlog(T_INFO, "clk_prepare return = %d\n", ret);
		ASSERT_INT_NE_GOTO(0, ret, err);
		clk_put(clk_fi);
	} else {
		tlog(T_INFO, "Clock unit_test_clk_1 get failed\n");
	}
	return;

err:
	terr("fault injection for clk_prepare failed\n");
	clk_put(clk_fi);
}

TEST(fault_injection_3, Inject_clk_set_rate)
{
	int ret = 0;

	tlog(T_INFO, "injecting error in virtio clock driver...\n");
	clk_fi = clk_get(dev_fi, "unit_test_clk_1");
	if (!IS_ERR_OR_NULL(clk_fi)) {
		ret = clk_set_rate(clk_fi, 400000);
		tlog(T_INFO, "clk unit_test_clk_1 set rate return %d\n", ret);
		ASSERT_INT_NE_GOTO(0, ret, err);
		clk_put(clk_fi);
	} else {
		tlog(T_INFO, "Clock unit_test_clk_1 get failed\n");
	}
	return;

err:
	terr("fault injection for clk_set_rate failed\n");
	clk_put(clk_fi);
}

TEST(fault_injection_4, Inject_clk_get_rate)
{
	int ret = 0;
	unsigned long rate_get;

	tlog(T_INFO, "injecting error in virtio clock driver...\n");
	clk_fi = clk_get(dev_fi, "unit_test_clk_1");
	if (!IS_ERR_OR_NULL(clk_fi)) {
		ret = clk_prepare_enable(clk_fi);
		EXPECT_INT_EQ(0, ret);
		ret = clk_set_rate(clk_fi, 400000);
		tlog(T_INFO, "clk unit_test_clk_1 set rate 400000 return %d\n", ret);
		rate_get = clk_get_rate(clk_fi);
		tlog(T_INFO, "clk unit_test_clk_1 get rate return %lu\n", rate_get);
		ASSERT_INT_GE_GOTO(1, rate_get, err);
		clk_put(clk_fi);
	} else {
		terr("Clock unit_test_clk_1 get failed\n");
	}

	return;

err:
	terr("fault injection for clk_get_rate failed\n");
	clk_put(clk_fi);
}

TEST(fault_injection_5, Inject_clk_round_rate)
{

	long round_rate;

	tlog(T_INFO, "injecting error in virtio clock driver...\n");
	clk_fi = clk_get(dev_fi, "unit_test_clk_1");
	if (!IS_ERR_OR_NULL(clk_fi)) {
		round_rate = clk_round_rate(clk_fi, 19200000);
		tlog(T_INFO, "clk unit_test_clk_1 round rate 19200000 return %ld\n", round_rate);
		ASSERT_INT_GE_GOTO(0, round_rate, err);
		clk_put(clk_fi);
	} else {
		terr("Clock unit_test_clk_1 get failed\n");
	}

	return;

err:
	terr("fault injection for clk_round_rate failed\n");
	clk_put(clk_fi);
}

TEST(fault_injection_6, Inject_clk_set_flags)
{
	int ret = 0;

	tlog(T_INFO, "injecting error in virtio clock driver...\n");
	clk_fi = clk_get(dev_fi, "unit_test_clk_45");
	ret = clk_set_flags(clk_fi, CLKFLAG_RETAIN_PERIPH);
	tlog(T_INFO, "clock unit_test_clk_45 set flag CLKFLAG_RETAIN_PERIPH return %d\n", ret);
	if (!IS_ERR_OR_NULL(clk_fi)) {
		ASSERT_INT_NE_GOTO(0, clk_set_flags(clk_fi, CLKFLAG_NORETAIN_MEM), err);
		clk_put(clk_fi);
	} else {
		tlog(T_INFO, "Clock unit_test_clk_45 get failed\n");
	}
	return;

err:
	terr("fault injection for clk_set_flags failed\n");
	clk_put(clk_fi);
}

TEST(fault_injection_7, Inject_reset_control_reset)
{
	int ret = 0;

	rst_fi = reset_control_get(dev_fi, "unit_test_reset_controller_1");
	if (!IS_ERR_OR_NULL(rst_fi)) {
		ret = reset_control_reset(rst_fi);
		ASSERT_INT_NE_GOTO(0, ret, err);
		reset_control_put(rst_fi);
	} else {
		tlog(T_INFO, "reset controller unit_test_reset_controller_1 get failed\n");
	}
	return;

err:
	terr("fault injection for reset_control_reset failed\n");
	reset_control_put(rst_fi);
}

TEST(fault_injection_8, Inject_reset_control_assert)
{
	int ret = 0;

	rst_fi = reset_control_get(dev_fi, "unit_test_reset_controller_1");
	if (!IS_ERR_OR_NULL(rst_fi)) {
		ret = reset_control_assert(rst_fi);
		ASSERT_INT_NE_GOTO(0, ret, err);
		reset_control_deassert(rst_fi);
		reset_control_put(rst_fi);
	} else {
		tlog(T_INFO, "reset controller unit_test_reset_controller_1 get failed\n");
	}

err:
	terr("fault injection for reset_control_assert failed\n");
	reset_control_put(rst_fi);
}

static const struct of_device_id virt_clk_test_dt_ids[] = {
	{ .compatible = "qcom,virtio-clock-unit-test" },
	{}
};

static int virt_clk_fault_inject_probe(struct platform_device *pdev)
{
	const struct of_device_id *id;
	tlog(T_INFO, "virt_clk_kprobe_test_probe\n");
	id = of_match_device(virt_clk_test_dt_ids, &pdev->dev);
	if (!id) {
		terr("No matching device found\n");
		return -ENODEV;
	}
	tlog(T_INFO, "of_match_device, &pdev->dev = 0x%x\n", &pdev->dev);

	dev_fi = &pdev->dev;

	return 0;
}

static int virt_clk_fault_inject_remove(struct platform_device *pdev)
{
	return 0;
}

static void add_tests(void)
{
	ADD_TEST(Inject_clk_get);
	ADD_TEST(Inject_clk_prepare);
	ADD_TEST(Inject_clk_set_rate);
	ADD_TEST(Inject_clk_get_rate);
	ADD_TEST(Inject_clk_round_rate);
	ADD_TEST(Inject_clk_set_flags);
	ADD_TEST(Inject_reset_control_reset);
	ADD_TEST(Inject_reset_control_assert);
}

static struct platform_driver virt_clk_test_driver = {
	.probe = virt_clk_fault_inject_probe,
	.remove = virt_clk_fault_inject_remove,
	.driver = {
		.name = DRV_NAME,
		.of_match_table = virt_clk_test_dt_ids,
	},
};

static int __init virtio_clk_fault_inject_init(void)
{
	int ret;
	tlog(T_INFO, "Fault injection kernel module for virtio clock initialising\n");

	add_tests();

	ret = platform_driver_register(&virt_clk_test_driver);
	if (ret)
		terr("Error registering platform driver\n");

	tlog(T_INFO, "Driver initialized\n");
	return ret;
}

static void __exit virtio_clk_fault_inject_exit(void)
{
	platform_driver_unregister(&virt_clk_test_driver);
	tlog(T_INFO, "virtio_clk fault injection kernel module Unloaded\n");
	KTF_CLEANUP();
}

module_init(virtio_clk_fault_inject_init);
module_exit(virtio_clk_fault_inject_exit);
MODULE_LICENSE("GPL v2");
