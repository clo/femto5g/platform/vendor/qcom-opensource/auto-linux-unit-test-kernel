#!/bin/bash
#
# Copyright (c) 2019, The Linux Foundation. All rights reserved.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 and
# only version 2 as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

symbol_1="of_clk_hw_virtio_get"
symbol_2="virtio_clk_prepare"
symbol_3="virtio_clk_set_rate"
symbol_4="virtio_clk_get_rate"
symbol_5="virtio_clk_round_rate"
symbol_6="virtio_clk_set_flags"
symbol_7="__virtio_reset"

offset_1=(0x134 0x6c)
offset_2=(0x920 0xa14 0xab4)
offset_3=(0xcc4 0xdb8 0xe58)
offset_4=(0x934 0xa28 0xac4 0xb50)
offset_5=(0xcc0 0xdb4 0xe50 0xedc)
offset_6=(0xcb0 0xda4 0xe40)
offset_7=(0xd68 0xe48 0xed4)

insmod fault_injection_virtio_clk_test.ko

# Fault injection for of_clk_hw_virtio_get
for i in ${offset_1[*]}
do
    echo "Run test case:Inject_clk_get  symbol:$symbol_1  offset:$i"
    insmod kprobe_unit_test.ko symbol=$symbol_1 offset=$i
    ktfrun --gtest_color=yes --gtest_filter=fault_injection_1.*
    sleep 5
    rmmod kprobe_unit_test.ko
done

# Fault injection for virtio_clk_prepare
for i in ${offset_2[*]}
do
    echo "Run test case:Inject_clk_prepare  symbol:$symbol_2  offset:$i"
    insmod kprobe_unit_test.ko symbol=$symbol_2 offset=$i
    ktfrun --gtest_color=yes --gtest_filter=fault_injection_2.*
    sleep 5
    rmmod kprobe_unit_test.ko
done

# Fault injection for virtio_clk_set_rate
for i in ${offset_3[*]}
do
    echo "Run test case:Inject_clk_set_rate  symbol:$symbol_3  offset:$i"
    insmod kprobe_unit_test.ko symbol=$symbol_3 offset=$i
    ktfrun --gtest_color=yes --gtest_filter=fault_injection_3.*
    sleep 5
    rmmod kprobe_unit_test.ko
done

# Fault injection for virtio_clk_get_rate
for i in ${offset_4[*]}
do
    echo "Run test case:Inject_clk_get_rate  symbol:$symbol_4  offset:$i"
    insmod kprobe_unit_test.ko symbol=$symbol_4 offset=$i
    ktfrun --gtest_color=yes --gtest_filter=fault_injection_4.*
    sleep 5
    rmmod kprobe_unit_test.ko
done

# Fault injection for virtio_clk_round_rate
for i in ${offset_5[*]}
do
    echo "Run test case:Inject_clk_prepare  symbol:$symbol_5  offset:$i"
    insmod kprobe_unit_test.ko symbol=$symbol_5 offset=$i
    ktfrun --gtest_color=yes --gtest_filter=fault_injection_5.*
    sleep 5
    rmmod kprobe_unit_test.ko
done

# Fault injection for virtio_clk_set_flags
for i in ${offset_6[*]}
do
    echo "Run test case:Inject_clk_prepare  symbol:$symbol_6  offset:$i"
    insmod kprobe_unit_test.ko symbol=$symbol_6 offset=$i
    ktfrun --gtest_color=yes --gtest_filter=fault_injection_6.*
    sleep 5
    rmmod kprobe_unit_test.ko
done

# Fault injection for __virtio_reset
for i in ${offset_7[*]}
do
    echo "Run test case:Inject_clk_prepare  symbol:$symbol_7  offset:$i"
    insmod kprobe_unit_test.ko symbol=$symbol_7 offset=$i
    ktfrun --gtest_color=yes --gtest_filter=fault_injection_7.*
    sleep 5
    rmmod kprobe_unit_test.ko
done

rmmod fault_injection_virtio_clk_test.ko
