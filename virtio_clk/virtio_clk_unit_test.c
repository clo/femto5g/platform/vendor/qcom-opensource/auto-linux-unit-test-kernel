/* Copyright (c) 2019, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <linux/clk.h>
#include <linux/clk/qcom.h>
#include <linux/platform_device.h>
#include <linux/module.h>
#include <linux/of_device.h>
#include <linux/kernel.h>
#include <linux/reset.h>
#include <linux/ktf.h>

KTF_INIT();

#define DRV_NAME	"virt_clk_test"
#define MAX_CLK     52
#define MAX_RESET      2

static struct device *dev_ut;
static struct clk *clk_ut;
static struct reset_control *rst_ut;

typedef struct virtio_clk_ctx {
	const char *clk_consumer_id[MAX_CLK];
	const char *reset_controller_consumer_id[MAX_RESET];
	char *invalide_consumer_id[5];
	unsigned long rate_value[10];
	long rate_outliers[3];
}virtio_clk_ctx;

static struct virtio_clk_ctx clk_test_ctx = {
	.clk_consumer_id = {
		"unit_test_clk_1",
		"unit_test_clk_2",
		"unit_test_clk_3",
		"unit_test_clk_4",
		"unit_test_clk_5",
		"unit_test_clk_6",
		"unit_test_clk_7",
		"unit_test_clk_8",
		"unit_test_clk_9",
		"unit_test_clk_10",
		"unit_test_clk_11",
		"unit_test_clk_12",
		"unit_test_clk_13",
		"unit_test_clk_14",
		"unit_test_clk_15",
		"unit_test_clk_16",
		"unit_test_clk_17",
		"unit_test_clk_18",
		"unit_test_clk_19",
		"unit_test_clk_20",
		"unit_test_clk_21",
		"unit_test_clk_22",
		"unit_test_clk_23",
		"unit_test_clk_24",
		"unit_test_clk_25",
		"unit_test_clk_26",
		"unit_test_clk_27",
		"unit_test_clk_28",
		"unit_test_clk_29",
		"unit_test_clk_30",
		"unit_test_clk_31",
		"unit_test_clk_32",
		"unit_test_clk_33",
		"unit_test_clk_34",
		"unit_test_clk_35",
		"unit_test_clk_36",
		"unit_test_clk_37",
		"unit_test_clk_38",
		"unit_test_clk_39",
		"unit_test_clk_40",
		"unit_test_clk_41",
		"unit_test_clk_42",
		"unit_test_clk_44",
		"unit_test_clk_45",
		"unit_test_clk_46",
		"unit_test_clk_47",
		"unit_test_clk_48",
		"unit_test_clk_49",
		"unit_test_clk_50",
		"unit_test_clk_51",
		"unit_test_clk_52",
	},
	.reset_controller_consumer_id = {
		"unit_test_reset_controller_2",
		"unit_test_reset_controller_7",
	},
	.invalide_consumer_id = { "gcc_gpu_cfg_ahb_clk", "mmssnoc_axi_clk", "apb_pclk", "\028", (char*) 0 },
	.rate_value = { 0, 400000, 1200000, 19200000, 27000000, 48000000,
						108000000, 200000000, 600000000, ULONG_MAX},
	.rate_outliers = { -1, -200000000, ULONG_MAX+1 },
};

enum invalid_clk_flags {
	CLKFLAG_TESTING = 6,
	CLKFLAG_NON_EXISTENT,
};

TEST(clock_handler, Test_clk_get)
{
	int i;

	/*Positive testing*/
	for (i = 0; i < MAX_CLK; i++) {
		tlog(T_INFO, "Clock name[%d] = %s\n", i, clk_test_ctx.clk_consumer_id[i]);
		clk_ut = clk_get(dev_ut, clk_test_ctx.clk_consumer_id[i]);
		tlog(T_INFO, "Clock Addr[%d] = 0x%x\n", i, clk_ut);
		ASSERT_OK_ADDR_GOTO(clk_ut, err);
		clk_put(clk_ut);
	}

	/*Negative testing*/
	clk_ut = clk_get(NULL, "unit_test_clk_1");
	EXPECT_INT_LE(PTR_ERR(clk_ut), 0);

	for (i = 0; i < 5; i++) {
		clk_ut = clk_get(dev_ut, clk_test_ctx.invalide_consumer_id[i]);
		EXPECT_INT_LT(PTR_ERR(clk_ut), 0);
	}

	return;

err:
	terr("Can't get clock %s\n", clk_test_ctx.clk_consumer_id[i]);
}

TEST(clock_handler, Test_clk_prepare_unprepare)
{
	int i;
	int ret = 0;

	/*Positive testing*/
	for (i = 0; i < MAX_CLK; i++) {
		clk_ut = clk_get(dev_ut, clk_test_ctx.clk_consumer_id[i]);
		if (!IS_ERR_OR_NULL(clk_ut)) {
			ret = clk_prepare(clk_ut);
			tlog(T_INFO, "clk_prepare[%d] return %d\n", i, ret);
			EXPECT_INT_EQ(0, ret);
			ret = clk_enable(clk_ut);
			tlog(T_INFO, "clk_enable[%d] return %d\n", i, ret);
			EXPECT_INT_EQ(0, ret);
			clk_disable(clk_ut);
			clk_unprepare(clk_ut);
			clk_put(clk_ut);
		} else {
			tlog(T_INFO, "Clock %s get failed\n", clk_test_ctx.clk_consumer_id[i]);
		}
	}

	/*Negative testing*/
	ret = clk_prepare(NULL);
	EXPECT_INT_LE(ret, 0);
}

TEST(clock_handler, Test_clk_set_get_rate)
{
	int i,j;
	int ret = 0;
	unsigned long get_rate;

	char *support_rate_set_clk[] = {
		"unit_test_clk_1",
		"unit_test_clk_2",
		"unit_test_clk_3",
		"unit_test_clk_4",
		"unit_test_clk_5",
		"unit_test_clk_6",
		"unit_test_clk_7",
		"unit_test_clk_8",
		"unit_test_clk_9",
		"unit_test_clk_10",
		"unit_test_clk_11",
		"unit_test_clk_12",
		"unit_test_clk_13",
		"unit_test_clk_14",
		"unit_test_clk_15",
		"unit_test_clk_16",
		"unit_test_clk_17",
		"unit_test_clk_18",
		"unit_test_clk_19",
		"unit_test_clk_20",
		"unit_test_clk_27",
		"unit_test_clk_33",
		"unit_test_clk_37",
		"unit_test_clk_42"
	};
	/*Positive testing*/
	for (i = 0; i < ARRAY_SIZE(support_rate_set_clk); i++) {
		clk_ut = clk_get(dev_ut, support_rate_set_clk[i]);
		if (!IS_ERR_OR_NULL(clk_ut)) {
			for (j = 0; j < 10; j++) {
				tlog(T_INFO, "Clock name[%d] = %s, set rate = %lu\n", i, support_rate_set_clk[i], clk_test_ctx.rate_value[j]);
				ret = clk_set_rate(clk_ut, clk_test_ctx.rate_value[j]);
				ASSERT_INT_EQ_GOTO(0, ret, err);
				get_rate = clk_get_rate(clk_ut);
				EXPECT_LONG_GE(get_rate, 0);
			}
			clk_put(clk_ut);
		} else {
			tlog(T_INFO, "Clock %s get failed\n", clk_test_ctx.clk_consumer_id[i]);
		}
	}

	/*Negative testing*/
	for (i = 0; i < ARRAY_SIZE(support_rate_set_clk); i++) {
		clk_ut = clk_get(dev_ut, support_rate_set_clk[i]);
		if (!IS_ERR_OR_NULL(clk_ut)) {
			for (j = 0; j < 3; j++) {
				EXPECT_INT_EQ(clk_set_rate(clk_ut, clk_test_ctx.rate_outliers[j]), 0);
			}
			clk_put(clk_ut);
		} else {
			tlog(T_INFO, "Clock %s get failed\n", clk_test_ctx.clk_consumer_id[i]);
		}
	}

	EXPECT_INT_LE(clk_set_rate(NULL, 1200000), 0);
	EXPECT_LONG_LE(clk_get_rate(NULL), 0);

	return;

err:
	terr("Clock %s set rate %lu failed\n", clk_test_ctx.clk_consumer_id[i], clk_test_ctx.rate_value[j]);
	clk_put(clk_ut);
}

TEST(clock_handler, Test_clk_round_rate)
{
	int i;
	long round_rate, round_invalid_rate, lowest_available, round_rate_step;
	/*Positive testing*/
	clk_ut = clk_get(dev_ut, "unit_test_clk_1");
	if (!IS_ERR_OR_NULL(clk_ut)) {
		lowest_available = clk_round_rate(clk_ut, 0);
		ASSERT_INT_GT_GOTO(lowest_available, 0, err);
		round_rate = clk_round_rate(clk_ut, 1200000);
		ASSERT_INT_GT_GOTO(round_rate, 0, err);
		round_rate_step = clk_round_rate(clk_ut, 27000000);
		ASSERT_INT_GT_GOTO(round_rate_step, 0, err);
		tlog(T_INFO, "Clock unit_test_clk_1 round rate 1200000 return %ld, 27000000 return %ld, lowest_available rate is %ld\n", round_rate, round_rate_step, lowest_available);
		EXPECT_LONG_GE(round_rate, lowest_available);
		EXPECT_LONG_GE(round_rate_step, round_rate);
	} else {
		tlog(T_INFO, "Clock unit_test_clk_1 get failed\n");
	}
	clk_put(clk_ut);

	/*Negative testing*/
	clk_ut = clk_get(dev_ut, "unit_test_clk_1");
	if (!IS_ERR_OR_NULL(clk_ut)) {
		for (i = 0; i < 3; i++) {
			tlog(T_INFO, "Clock unit_test_clk_1 round invalide rate %ld\n", clk_test_ctx.rate_outliers[i]);
			round_invalid_rate = clk_round_rate(clk_ut, clk_test_ctx.rate_outliers[i]);
			EXPECT_LONG_GE(round_invalid_rate, 0);
		}
	} else {
		tlog(T_INFO, "Clock unit_test_clk_1 get failed\n");
	}
	clk_put(clk_ut);
	round_rate = clk_round_rate(NULL, 1200000);
	EXPECT_LONG_LE(round_rate, 0);

	return;

err:
	terr("Unable to round clk\n");
	clk_put(clk_ut);
}

TEST(clock_handler, Test_clk_set_flags)
{
	int i, ret;
	char *flags_clk[] = {
		"unit_test_clk_45",
		"unit_test_clk_46",
	};
	/*Positive testing*/
	for (i = 0; i < ARRAY_SIZE(flags_clk); i++) {
		tlog(T_INFO, "Clock name[%d] = %s", i, flags_clk[i]);
		clk_ut = clk_get(dev_ut, flags_clk[i]);
		if (!IS_ERR_OR_NULL(clk_ut)) {
			EXPECT_INT_EQ(0, clk_set_flags(clk_ut, CLKFLAG_NORETAIN_MEM));
			EXPECT_INT_EQ(0, clk_set_flags(clk_ut, CLKFLAG_NORETAIN_PERIPH));
			clk_put(clk_ut);
		} else {
			tlog(T_INFO, "Clock %s get failed\n", clk_test_ctx.clk_consumer_id[i]);
		}
	}

	/*Negative testing*/
	for (i = 0; i < MAX_CLK; i++) {
		clk_ut = clk_get(dev_ut, clk_test_ctx.clk_consumer_id[i]);
		if (!IS_ERR_OR_NULL(clk_ut)) {
			EXPECT_INT_LT(clk_set_flags(clk_ut, CLKFLAG_TESTING), 0);
			EXPECT_INT_LT(clk_set_flags(clk_ut, CLKFLAG_NON_EXISTENT), 0);
			clk_put(clk_ut);
		} else {
			tlog(T_INFO, "Clock %s get failed\n", clk_test_ctx.clk_consumer_id[i]);
		}
	}

	ret = clk_set_flags(NULL, CLKFLAG_RETAIN_PERIPH);
	EXPECT_INT_LE(ret, 0);

}

TEST(clock_reset, Test_reset_control_get)
{
	int i;
	/*Positive testing*/
	for (i = 0; i < MAX_RESET; i++) {
		tlog(T_INFO, "Reset controller [%d] = %s\n", i, clk_test_ctx.reset_controller_consumer_id[i]);
		rst_ut = reset_control_get(dev_ut, clk_test_ctx.reset_controller_consumer_id[i]);
		tlog(T_INFO, "Addr = 0x%x\n", rst_ut);
		ASSERT_OK_ADDR_GOTO(rst_ut, err);
		reset_control_put(rst_ut);
	}

	/*Negative testing*/
	for (i = 0; i < 5; i++) {
		rst_ut = reset_control_get(dev_ut, clk_test_ctx.invalide_consumer_id[i]);
		EXPECT_INT_LT(PTR_ERR(rst_ut), 0);
	}
	return;

err:
	terr("Can't get reset %s\n", clk_test_ctx.reset_controller_consumer_id[i]);
}

TEST(clock_reset, Test_reset_control_reset)
{
	int i;
	int ret = 0;
	/*Positive testing*/
	for (i = 0; i < MAX_RESET; i++) {
		rst_ut = reset_control_get(dev_ut, clk_test_ctx.reset_controller_consumer_id[i]);
		if (!IS_ERR_OR_NULL(rst_ut)) {
			ret = reset_control_reset(rst_ut);
			EXPECT_INT_EQ(0, ret);
			reset_control_put(rst_ut);
		} else {
			tlog(T_INFO, "reset controller %s get failed\n", clk_test_ctx.reset_controller_consumer_id[i]);
		}
	}

	/*Negative testing*/
	ret = reset_control_reset(NULL);
	EXPECT_INT_LE(ret, 0);
}

TEST(clock_reset, Test_reset_control_assert)
{
	int i;
	int ret = 0;
	/*Positive testing*/
	for (i = 0; i < MAX_RESET; i++) {
		rst_ut = reset_control_get(dev_ut, clk_test_ctx.reset_controller_consumer_id[i]);
		if (!IS_ERR_OR_NULL(rst_ut)) {
			ret = reset_control_assert(rst_ut);
			ASSERT_INT_EQ_GOTO(0, ret, err);
			reset_control_deassert(rst_ut);
			reset_control_put(rst_ut);
		}
	}

	/*Negative testing*/
	ret = reset_control_assert(NULL);
	EXPECT_INT_LE(ret, 0);

	return;

err:
	terr("Fail to assert %s reset\n", clk_test_ctx.reset_controller_consumer_id[i]);
	reset_control_put(rst_ut);
}

TEST(clock_reset, Test_reset_control_deassert)
{
	int i;
	int ret = 0;

	/*Positive testing*/
	for (i = 0; i < MAX_RESET; i++) {
		rst_ut = reset_control_get(dev_ut, clk_test_ctx.reset_controller_consumer_id[i]);
		if (!IS_ERR_OR_NULL(rst_ut)) {
			ret = reset_control_assert(rst_ut);
			if (!ret) {
				ret = reset_control_deassert(rst_ut);
				ASSERT_INT_EQ_GOTO(0, ret, err);
				reset_control_put(rst_ut);
			}
		}
	}

	/*Negative testing*/
	ret = reset_control_assert(NULL);
	EXPECT_INT_LE(ret, 0);

	return;

err:
	terr("Fail to deassert %s reset\n", clk_test_ctx.reset_controller_consumer_id[i]);
	reset_control_put(rst_ut);
}

TEST(clock_reset, Test_reset_control_status)
{
	int i;
	int ret = 0;

	/*Positive testing*/
	rst_ut = reset_control_get(dev_ut, "unit_test_reset_controller_1");
	if (!IS_ERR_OR_NULL(rst_ut)) {
		if(!reset_control_assert(rst_ut)) {
			ret = reset_control_status(rst_ut);
			ASSERT_INT_GT_GOTO(ret, 0, err);
			ret = reset_control_deassert(rst_ut);
			if (!ret) {
				EXPECT_INT_EQ(0, reset_control_status(rst_ut));
				reset_control_put(rst_ut);
			}
		}
	}

	/*Negative testing*/
	ret = reset_control_status(NULL);
	EXPECT_INT_LE(ret, 0);

	return;

err:
	terr("Fail to query %s reset status\n", clk_test_ctx.reset_controller_consumer_id[i]);
	reset_control_put(rst_ut);
}


static const struct of_device_id virt_clk_test_dt_ids[] = {
	{ .compatible = "qcom,virtio-clock-unit-test" },
	{}
};

static int virt_clk_test_probe(struct platform_device *pdev)
{
	const struct of_device_id *id;
	tlog(T_INFO, "virt_clk_test_probe\n");
	id = of_match_device(virt_clk_test_dt_ids, &pdev->dev);
	if (!id) {
		terr("No matching device found\n");
		return -ENODEV;
	}
	tlog(T_INFO, "of_match_device, &pdev->dev = 0x%x\n", &pdev->dev);

	dev_ut = &pdev->dev;

	return 0;
}

static int virt_clk_test_remove(struct platform_device *pdev)
{
	return 0;
}

static void add_tests(void)
{
	ADD_TEST(Test_clk_get);
	ADD_TEST(Test_clk_prepare_unprepare);
	ADD_TEST(Test_clk_set_get_rate);
	ADD_TEST(Test_clk_round_rate);
	ADD_TEST(Test_clk_set_flags);
	ADD_TEST(Test_reset_control_get);
	ADD_TEST(Test_reset_control_reset);
	ADD_TEST(Test_reset_control_assert);
	ADD_TEST(Test_reset_control_deassert);
	ADD_TEST(Test_reset_control_status);
}

static struct platform_driver virt_clk_test_driver = {
	.probe = virt_clk_test_probe,
	.remove = virt_clk_test_remove,
	.driver = {
		.name = DRV_NAME,
		.of_match_table = virt_clk_test_dt_ids,
	},
};

static int __init virtio_clk_ut_init(void)
{
	int ret;
	tlog(T_INFO, "Unit test kernel module for virtio clock initialising\n");

	add_tests();

	ret = platform_driver_register(&virt_clk_test_driver);
	if (ret)
		terr("Error registering platform driver\n");

	tlog(T_INFO, "Driver initialized\n");
	return ret;
}

static void __exit virtio_clk_ut_exit(void)
{
	platform_driver_unregister(&virt_clk_test_driver);
	tlog(T_INFO, "virtio_clk unit test kernel module Unloaded\n");
	KTF_CLEANUP();
}

module_init(virtio_clk_ut_init);
module_exit(virtio_clk_ut_exit);
MODULE_LICENSE("GPL v2");
