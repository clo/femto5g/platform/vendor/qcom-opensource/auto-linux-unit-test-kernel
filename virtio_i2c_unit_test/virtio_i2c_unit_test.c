/* Copyright (c) 2020, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
#include <linux/i2c.h>
#include <linux/module.h>
#include <linux/of_device.h>
#include <linux/kernel.h>
#include <linux/ktf.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>

KTF_INIT();
#define DRV_NAME	"virt_i2c_test"
static struct i2c_client *client_ut = NULL;
static struct i2c_adapter *adapter_ut = NULL;
static struct i2c_msg  msgs_ut[2];
static char buf1[125] = "abcdefghigklmn";
static char buf2[6000] = "abcdefghigklmn";
int ret = 0;

TEST(i2c_handler, Test_i2c_master_send)
{
	client_ut->flags = 0;
	client_ut->addr = 0x36;
	/* Positive testing */
	tlog(T_INFO, "1. Test_i2c_master_send Positive testing : size_bytes is 125\n");
	ret = i2c_master_send(client_ut, buf1, 125);
	EXPECT_INT_EQ(125, ret);
	tlog(T_INFO, "1. Test_i2c_master_send Positive testing : size_bytes is 125 ret=%d\n", ret);

	tlog(T_INFO, "2. Test_i2c_master_send Positive testing : size_bytes is 6000\n");
	ret = i2c_master_send(client_ut, buf2, 6000);
	EXPECT_INT_EQ(6000, ret);
	tlog(T_INFO, "2. Test_i2c_master_send Positive testing : size_bytes is 6000 ret=%d\n", ret);

	/*Negative testing*/
	/*1.input invalide data buffer pointer or NULL*/
	tlog(T_INFO, "1.Test_i2c_master_send Negative testing : input  data buffer is NULL\n");
	ret = i2c_master_send(client_ut, NULL, 25);
	EXPECT_INT_LT(ret, 0);
	tlog(T_INFO, "1.Test_i2c_master_send Negative testing : buffer is NULL ret=%d\n", ret);

	/*2.input invalide addr in device pointer*/
	tlog(T_INFO, "2.Test_i2c_master_send Negative testing : input invalide addr in device pointer\n");
	client_ut->addr = 0x30;
	ret = i2c_master_send(client_ut, buf1, 125);
	EXPECT_INT_LT(ret, 0);
	tlog(T_INFO, "2.Test_i2c_master_send Negative testing : input invalide addr in device pointer ret=%d\n", ret);
}

TEST(i2c_handler, Test_I2c_master_recv)
{
	client_ut->addr = 0x36;
	/*Positive testing*/
	client_ut->flags = 1;
	tlog(T_INFO, "1. Test_I2c_master_recv Positive testing : size_bytes is 125\n");
	ret = i2c_master_recv(client_ut, buf1, 125);
	EXPECT_INT_EQ(125, ret);
	tlog(T_INFO, "1. Test_I2c_master_recv Positive testing : size_bytes is 125 ret=%d\n", ret);

	/*Negative testing*/
	/*1.input invalide data buffer pointer or NULL*/
	client_ut->flags = 1;
	tlog(T_INFO, "1. Test_I2c_master_recv Negative testing :input invalide data buffer is NULL\n");
	ret=i2c_master_recv(client_ut, NULL, 125);
	EXPECT_INT_LT(ret, 0);
	tlog(T_INFO, "1. Test_I2c_master_recv Negative testing : input invalide data buffer is NULL ret=%d\n", ret);

	/*2.input invalide data buffer size*/
	client_ut->flags=1;
	tlog(T_INFO, "2. Test_I2c_master_recv Negative testing : input invalide data buffer size\n");
	ret=i2c_master_recv(client_ut, buf1, 6000);
	EXPECT_INT_LT(ret, 0);
	tlog(T_INFO, "2. Test_I2c_master_recv Negative testing : input invalide data buffer size ret=%d\n", ret);

	/*3.input invalide addr in device pointer*/
	client_ut->flags = 1;
	client_ut->addr = 0x30;
	tlog(T_INFO, "3. Test_I2c_master_recv Negative testing : input invalide addr in device pointer\n");
	ret = i2c_master_recv(client_ut, buf1, 125);
	EXPECT_INT_LT(ret, 0);
	tlog(T_INFO, "3. Test_I2c_master_recv Negative testing : input invalide addr in device pointer ret=%d\n", ret);
}

TEST(i2c_handler, Test_i2c_transfer)
{
	uint8_t send_buf, recv_buf;
	msgs_ut[0].addr = 0x36;
	msgs_ut[0].len = sizeof(recv_buf);
	msgs_ut[0].buf = &recv_buf;
	msgs_ut[0].flags = 1;

	msgs_ut[1].addr = 0x36;
	msgs_ut[1].len = sizeof(send_buf);
	msgs_ut[1].buf = &send_buf;
	msgs_ut[1].flags = 0;
	/*Positive testing*/
	tlog(T_INFO, "1. Test_i2c_transfer Positive testing : read \n");
	ret = i2c_transfer(adapter_ut, &msgs_ut[0], 1);
	tlog(T_INFO, "1. Test_i2c_transfer Positive testing : read ret=%d\n", ret);
	EXPECT_INT_EQ(1, ret);

	tlog(T_INFO, "2. Test_i2c_transfer Positive testing : write \n");
	ret = i2c_transfer(adapter_ut, &msgs_ut[1], 1);
	tlog(T_INFO, "2. Test_i2c_transfer Positive testing : write ret=%d\n", ret);
	EXPECT_INT_EQ(1, ret);

	tlog(T_INFO, "3. Test_i2c_transfer Positive testing : write_read \n");
	ret = i2c_transfer(adapter_ut, msgs_ut, 2);
	EXPECT_INT_EQ(2, ret);
	tlog(T_INFO, "3. Test_i2c_transfer Positive testing : write_read ret=%d\n", ret);

	/*Negative testing*/
	/*1.input invalide msg pointer or NULL*/
	tlog(T_INFO, "1.Test_i2c_transfer Negative testing : input invalide msg pointer or NULL\n");
	ret = i2c_transfer(adapter_ut, NULL, 1);
	EXPECT_INT_LT(ret, 0);
	tlog(T_INFO, "1.Test_i2c_transfer Negative testing : input invalide msg pointer or NULL ret=%d\n", ret);

	/*2.input invalide msg number*/
	tlog(T_INFO, "2.Test_i2c_transfer Negative testing : input invalide msg number\n");
	ret = i2c_transfer(adapter_ut, &msgs_ut[1], 10);
	EXPECT_INT_LT(ret, 0);
	tlog(T_INFO, "2.Test_i2c_transfer Negative testing : input invalide msg number ret=%d\n", ret);

	/*3.Read: input invalide addr in i2c bus pointer*/
	msgs_ut[0].addr = 0x30;
	tlog(T_INFO, "3.Test_i2c_transfer Negative testing : Read: input invalide addr in i2c bus pointer\n");
	ret = i2c_transfer(adapter_ut, &msgs_ut[0], 1);
	EXPECT_INT_LT(ret, 0);
	tlog(T_INFO, "3.Test_i2c_transfer Negative testing : Read: input invalide addr in i2c bus pointer ret=%d\n", ret);

	/*4.Write_read: input invalide addr in i2c bus pointer*/
	msgs_ut[1].addr = 0x30;
	msgs_ut[0].addr = 0x30;
	tlog(T_INFO, "4.Test_i2c_transfer Negative testing : Write_read: input invalide addr in i2c bus pointer\n");
	ret=i2c_transfer(adapter_ut, msgs_ut, 2);
	EXPECT_INT_LT(ret, 0);
	tlog(T_INFO, "4.Test_i2c_transfer Negative testing : Write_read: input invalide addr in i2c bus pointer ret=%d\n", ret);
}



static int virt_i2c_test_probe(struct i2c_client *client,
                const struct i2c_device_id *id)
{
	tlog(T_INFO, "virt_i2c_test_probe\n");
	client_ut = client;
	adapter_ut = client_ut->adapter;
	return 0;
}

static int virt_i2c_test_remove(struct i2c_client *client)
{
	return 0;
}

static void add_tests(void)
{
	ADD_TEST(Test_i2c_master_send);
	ADD_TEST(Test_I2c_master_recv);
	ADD_TEST(Test_i2c_transfer);
}
static const struct i2c_device_id adark_id[] = {
	{DRV_NAME, 0},
	{},
};

MODULE_DEVICE_TABLE(i2c, adark_id);

static const struct of_device_id virt_i2c_test_dt_ids[] = {
	{ .compatible = "qcom,i2c-unit-test" },
	{},
};

MODULE_DEVICE_TABLE(of, virt_i2c_test_dt_ids);

static struct i2c_driver virt_i2c_test_driver = {
	.driver = {
		.owner = THIS_MODULE,
		.name = DRV_NAME,
		.of_match_table = virt_i2c_test_dt_ids,
	},
	.class = I2C_CLASS_HWMON,
	.id_table = adark_id,
	.probe = virt_i2c_test_probe,
	.remove = virt_i2c_test_remove,
};



static int __init virtio_i2c_ut_init(void)
{
	int ret;
	tlog(T_INFO, "Unit test kernel module for virtio i2c initialising\n");

	add_tests();
	ret = i2c_add_driver(&virt_i2c_test_driver);

	tlog(T_INFO, "Driver initialized\n");
	return ret;
}

static void __exit virtio_i2c_ut_exit(void)
{
	i2c_del_driver(&virt_i2c_test_driver);
	tlog(T_INFO, "virtio_i2c unit test kernel module Unloaded\n");
	KTF_CLEANUP();
}

module_init(virtio_i2c_ut_init);
module_exit(virtio_i2c_ut_exit);
MODULE_LICENSE("GPL v2");
