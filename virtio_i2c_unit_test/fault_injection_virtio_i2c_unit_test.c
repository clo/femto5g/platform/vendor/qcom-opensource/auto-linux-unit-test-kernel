/* Copyright (c) 2020, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
#include <linux/i2c.h>
#include <linux/module.h>
#include <linux/of_device.h>
#include <linux/kernel.h>
#include <linux/ktf.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>

KTF_INIT();
#define DRV_NAME	"virt_i2c_test"
static struct i2c_client *client_ut = NULL;
static struct i2c_adapter *adapter_ut = NULL;
static struct i2c_msg  msgs_ut[1];
int ret = 0;


TEST(i2c_handler, Test_i2c_transfer)
{
	uint8_t send_buf, recv_buf;
	msgs_ut[0].addr = 0x36;
	msgs_ut[0].len = sizeof(recv_buf);
	msgs_ut[0].buf = &recv_buf;
	msgs_ut[0].flags = 1;

	/*fault injection testing*/
	tlog(T_INFO, " Test_i2c_transfer fault injection testing : read \n");
	ret = i2c_transfer(adapter_ut, &msgs_ut[0], 1);
	tlog(T_INFO, " Test_i2c_transfer fault injection testing : read ret = %d\n", ret);
	EXPECT_INT_LT(ret, 0);

}

static int virt_i2c_test_probe(struct i2c_client *client,
                const struct i2c_device_id *id)
{
	tlog(T_INFO, "virt_i2c_test_probe\n");
	client_ut = client;
	adapter_ut = client_ut->adapter;
	return 0;
}

static int virt_i2c_test_remove(struct i2c_client *client)
{
	return 0;
}

static void add_tests(void)
{
	ADD_TEST(Test_i2c_transfer);
}
static const struct i2c_device_id adark_id[] = {
	{DRV_NAME, 0},
	{},
};

MODULE_DEVICE_TABLE(i2c, adark_id);

static const struct of_device_id virt_i2c_test_dt_ids[] = {
	{ .compatible = "qcom,i2c-unit-test" },
	{},
};

MODULE_DEVICE_TABLE(of, virt_i2c_test_dt_ids);

static struct i2c_driver virt_i2c_test_driver = {
	.driver = {
		.owner = THIS_MODULE,
		.name = DRV_NAME,
		.of_match_table = virt_i2c_test_dt_ids,
	},
	.class = I2C_CLASS_HWMON,
	.id_table = adark_id,
	.probe = virt_i2c_test_probe,
	.remove = virt_i2c_test_remove,
};



static int __init virtio_i2c_ut_init(void)
{
	int ret;
	tlog(T_INFO, "Unit test kernel module for virtio i2c initialising\n");

	add_tests();
	ret = i2c_add_driver(&virt_i2c_test_driver);

	tlog(T_INFO, "Driver initialized\n");
	return ret;
}

static void __exit virtio_i2c_ut_exit(void)
{
	i2c_del_driver(&virt_i2c_test_driver);
	tlog(T_INFO, "virtio_i2c unit test kernel module Unloaded\n");
	KTF_CLEANUP();
}

module_init(virtio_i2c_ut_init);
module_exit(virtio_i2c_ut_exit);
MODULE_LICENSE("GPL v2");
