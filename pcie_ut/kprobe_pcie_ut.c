/* Copyright (c) 2020, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/kprobes.h>
#include <asm/ptrace.h>

#define MAX_PROBES 6

static char *symbol[MAX_PROBES];
static int symbol_num;
static int offset[MAX_PROBES];

module_param_array(symbol, charp, &symbol_num, 0);
MODULE_PARM_DESC(symbol, "The probe symbol for the kprobe injection.");

module_param_array(offset, int, NULL, 0);
MODULE_PARM_DESC(symbol, "The offset value starting from symbol for the kprobe injection.");

struct probe_point {
	char *api;
	int offset;
	int reg;
	int value;
};

/*
 * msm_pcie_get_resources() 0x2d5dc
 * dev->tcsr_config = (struct msm_pcie_tcsr_info_t *)
 * 0x2eee0: f9042380 str x0, [x28, #2112]
 * 0x2eee0 - 0x2d5dc = 0x1904
 */
static const struct probe_point probe_pcie = {"msm_pcie_get_resources", 0x1904, 0, 0};

static int kp_index;
static int kp_value;

/* For each probe you need to allocate a kprobe structure */
static struct kprobe kp;

/* kprobe pre_handler: called just before the probed instruction is executed */
static int kp_pre_handler(struct kprobe *p, struct pt_regs *regs)
{
	printk(KERN_INFO "%s: p->addr=0x%pK, x[%d]=0x%llx\n", __func__,
			p->addr, kp_index, regs->user_regs.regs[kp_index]);
	regs->user_regs.regs[kp_index] = kp_value;
	/* A dump_stack() here will give a stack backtrace */
	dump_stack();
	return 0;
}


/* kprobe post_handler: called after the probed instruction is executed */
static void kp_post_handler(struct kprobe *p, struct pt_regs *regs,
				unsigned long flags)
{
	printk(KERN_INFO "%s: p->addr=0x%pK, x[%d]=0x%llx\n",
			__func__, p->addr, kp_index, regs->user_regs.regs[kp_index]);
	regs->user_regs.regs[kp_index] = kp_value;
}


/*
 * fault_handler: this is called if an exception is generated for any
 * instruction within the pre- or post-handler, or when Kprobes
 * single-steps the probed instruction.
 */
static int kp_fault_handler(struct kprobe *p, struct pt_regs *regs, int trapnr)
{
	printk(KERN_INFO "fault_handler: p->addr = 0x%p, trap #%d\n",
		p->addr, trapnr);
	/* Return 0 because we don't handle the fault. */
	return 0;
}

static int __init kprobe_init(void)
{
	int ret;
	kprobe_opcode_t *symbol_addr;

        kp.symbol_name = probe_pcie.api;
        kp.offset = probe_pcie.offset;
        kp.pre_handler = kp_pre_handler;
        kp.post_handler = kp_post_handler;
        kp.fault_handler = kp_fault_handler;
        kp_index = probe_pcie.reg;
        kp_value = probe_pcie.value;

	symbol_addr = (kprobe_opcode_t *)kallsyms_lookup_name(kp.symbol_name);
	if (!symbol_addr) {
		printk(KERN_INFO "Could not find address of %s\n", kp.symbol_name);
		return -1;
	}
	printk(KERN_INFO "kp.symbol_addr = 0x%x\n", symbol_addr);

        ret = register_kprobe(&kp);
        if (ret < 0) {
                printk(KERN_INFO "register_kprobe failed, returned %d\n", ret);
                return ret;
        }
        printk(KERN_INFO "Planted kprobe: %s+%pK at %pK\n", kp.symbol_name, (kprobe_opcode_t *)kp.offset, (kprobe_opcode_t *)kp.addr);
	return 0;
}

static void __exit kprobe_exit(void)
{
        unregister_kprobe(&kp);
        printk(KERN_INFO "kprobe at %pK unregistered\n", kp.addr);
}

module_init(kprobe_init)
module_exit(kprobe_exit)
MODULE_LICENSE("GPL v2");
