/* Copyright (c) 2020, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
#include <linux/module.h>
#include <linux/ktf.h>
#include <asm/io.h>

MODULE_LICENSE("GPL v2");

KTF_INIT();


TEST(PCIe_ut, PCIe_unit_test)
{
	unsigned int *tcsr2 = ioremap(0x01fec004,0x04);
	unsigned int PCIe_RC2_tscr;
	PCIe_RC2_tscr = readl(tcsr2);
	tlog(T_INFO, "PCIe_RC2 tscr is: %d\n", PCIe_RC2_tscr);
	EXPECT_INT_EQ(PCIe_RC2_tscr, 1);
}

static void add_tests(void)
{
	ADD_TEST(PCIe_unit_test);
}

static int __init PCIe_init(void)
{
	add_tests();
	tlog(T_INFO, "PCIe: loaded");

	return 0;
}

static void __exit PCIe_exit(void)
{
	KTF_CLEANUP();
	tlog(T_INFO, "PCIe: unloaded");
}

module_init(PCIe_init);
module_exit(PCIe_exit);
