/* Copyright (c) 2019, The Linux Foundation. All rights reserved.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 and
 * only version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

#include <linux/regulator/consumer.h>
#include <linux/regulator/driver.h>
#include <linux/platform_device.h>
#include <linux/module.h>
#include <linux/of_device.h>
#include <linux/kernel.h>
#include <linux/reset.h>
#include <linux/ktf.h>

KTF_INIT();

#define DRV_NAME	"virt_reg_fault_inject_test"

static struct device *dev_fi;
static struct regulator *reg_fi;

TEST(fault_injection_1, Inject_regulator_enable)
{
	int ret = 0;

	tlog(T_INFO, "injecting error in virtio regulator driver...\n");
	reg_fi = regulator_get(dev_fi, "unit_test_reg_11");
	if (!IS_ERR_OR_NULL(reg_fi)) {
		ret = regulator_enable(reg_fi);
		tlog(T_INFO, "regulator_enable return = %d\n", ret);
		ASSERT_INT_NE_GOTO(0, ret, err);
		regulator_put(reg_fi);
	} else {
		tlog(T_INFO, "Regulator unit_test_reg_11 get failed\n");
	}
	return;

err:
	terr("fault injection for regulator_enable failed\n");
	regulator_put(reg_fi);
}

TEST(fault_injection_2, Inject_regulator_disable)
{
	int ret = 0;

	tlog(T_INFO, "injecting error in virtio regulator driver...\n");
	reg_fi = regulator_get(dev_fi, "unit_test_reg_11");
	if (!IS_ERR_OR_NULL(reg_fi)) {
		ret = regulator_enable(reg_fi);
		EXPECT_INT_EQ(0, ret);
		ret = regulator_disable(reg_fi);
		tlog(T_INFO, "regulator_disable return = %d\n", ret);
		ASSERT_INT_NE_GOTO(0, ret, err);
		regulator_put(reg_fi);
	} else {
		tlog(T_INFO, "Regulator unit_test_reg_11 get failed\n");
	}
	return;

err:
	terr("fault injection for regulator_disable failed\n");
	regulator_put(reg_fi);
}

TEST(fault_injection_3, Inject_regulator_set_voltage)
{
	int ret = 0;

	tlog(T_INFO, "injecting error in virtio regulator driver...\n");
	reg_fi = regulator_get(dev_fi, "unit_test_reg_12");
	if (!IS_ERR_OR_NULL(reg_fi)) {
		ret = regulator_set_voltage(reg_fi, 800000, 800000);
		tlog(T_INFO, "regulator unit_test_reg_12 set rate return %d\n", ret);
		ASSERT_INT_NE_GOTO(0, ret, err);
		regulator_put(reg_fi);
	} else {
		tlog(T_INFO, "Regulator unit_test_reg_12 get failed\n");
	}
	return;

err:
	terr("fault injection for regulator_set_voltage failed\n");
	regulator_put(reg_fi);
}

TEST(fault_injection_4, Inject_regulator_get_voltage)
{
	int vout;
	int ret = 0;

	tlog(T_INFO, "injecting error in virtio regulator driver...\n");
	reg_fi = regulator_get(dev_fi, "unit_test_reg_12");
	if (!IS_ERR_OR_NULL(reg_fi)) {
		ret = regulator_set_voltage(reg_fi, 800000, 800000);
		EXPECT_INT_EQ(0, ret);
		ret = regulator_enable(reg_fi);
		EXPECT_INT_EQ(0, ret);
		vout = regulator_get_voltage(reg_fi);
		ASSERT_INT_LT_GOTO(vout, 800000, err);
		ret = regulator_disable(reg_fi);
		EXPECT_INT_EQ(0, ret);
		regulator_put(reg_fi);
	} else {
		tlog(T_INFO, "Regulator unit_test_reg_12 get failed\n");
	}
	return;

err:
	terr("fault injection for regulator_get_voltage failed\n");
	regulator_put(reg_fi);
}

TEST(fault_injection_5, Inject_regulator_set_mode)
{
	int ret = 0;

	tlog(T_INFO, "injecting error in virtio regulator driver...\n");
	reg_fi = regulator_get(dev_fi, "unit_test_reg_1");
	if (!IS_ERR_OR_NULL(reg_fi)) {
		ret = regulator_enable(reg_fi);
		EXPECT_INT_EQ(0, ret);
		ret = regulator_set_mode(reg_fi, REGULATOR_MODE_NORMAL);
		tlog(T_INFO, "regulator_set_mode return = %d\n", ret);
		ASSERT_INT_NE_GOTO(0, ret, err);
		ret = regulator_disable(reg_fi);
		EXPECT_INT_EQ(0, ret);
		regulator_put(reg_fi);
	} else {
		tlog(T_INFO, "Regulator unit_test_reg_1 get failed\n");
	}
	return;

err:
	terr("fault injection for regulator_set_mode failed\n");
	regulator_put(reg_fi);
}

TEST(fault_injection_6, Inject_regulator_get_mode)
{
	unsigned int mode;
	int ret = 0;

	tlog(T_INFO, "injecting error in virtio regulator driver...\n");
	reg_fi = regulator_get(dev_fi, "unit_test_reg_1");
	if (!IS_ERR_OR_NULL(reg_fi)) {
		ret = regulator_enable(reg_fi);
		EXPECT_INT_EQ(0, ret);
		ret = regulator_set_mode(reg_fi, REGULATOR_MODE_NORMAL);
		EXPECT_INT_NE(0, ret);
		mode = regulator_get_mode(reg_fi);
		tlog(T_INFO, "regulator_get_mode return = %u\n", mode);
		ASSERT_INT_NE_GOTO(0x2, mode, err);
		ret = regulator_disable(reg_fi);
		EXPECT_INT_EQ(0, ret);
		regulator_put(reg_fi);
	} else {
		tlog(T_INFO, "Regulator unit_test_reg_1 get failed\n");
	}
	return;

err:
	terr("fault injection for regulator_get_mode failed\n");
	regulator_put(reg_fi);
}

TEST(fault_injection_7, Inject_regulator_set_load)
{
	int ret = 0;

	tlog(T_INFO, "injecting error in virtio regulator driver...\n");
	reg_fi = regulator_get(dev_fi, "unit_test_reg_10");
	if (!IS_ERR_OR_NULL(reg_fi)) {
		ret = regulator_enable(reg_fi);
		EXPECT_INT_EQ(0, ret);
		ret = regulator_set_load(reg_fi, 200000);
		tlog(T_INFO, "regulator_set_lode return = %d\n", ret);
		ASSERT_INT_NE_GOTO(0, ret, err);
		ret = regulator_disable(reg_fi);
		EXPECT_INT_EQ(0, ret);
		regulator_put(reg_fi);
	} else {
		tlog(T_INFO, "Regulator unit_test_reg_10 get failed\n");
	}
	return;

err:
	terr("fault injection for regulator_set_lode failed\n");
	regulator_put(reg_fi);
}

static const struct of_device_id virt_reg_test_dt_ids[] = {
	{ .compatible = "qcom,virtio-regulator-unit-test" },
	{}
};

static int virt_reg_fault_inject_probe(struct platform_device *pdev)
{
	const struct of_device_id *id;
	tlog(T_INFO, "virt_reg_kprobe_test_probe\n");
	id = of_match_device(virt_reg_test_dt_ids, &pdev->dev);
	if (!id) {
		terr("No matching device found\n");
		return -ENODEV;
	}
	tlog(T_INFO, "of_match_device, &pdev->dev = 0x%x\n", &pdev->dev);

	dev_fi = &pdev->dev;

	return 0;
}

static int virt_reg_fault_inject_remove(struct platform_device *pdev)
{
	return 0;
}

static void add_tests(void)
{
	ADD_TEST(Inject_regulator_enable);
	ADD_TEST(Inject_regulator_disable);
	ADD_TEST(Inject_regulator_set_voltage);
	ADD_TEST(Inject_regulator_get_voltage);
	ADD_TEST(Inject_regulator_set_mode);
	ADD_TEST(Inject_regulator_get_mode);
	ADD_TEST(Inject_regulator_set_load);
}

static struct platform_driver virt_reg_test_driver = {
	.probe = virt_reg_fault_inject_probe,
	.remove = virt_reg_fault_inject_remove,
	.driver = {
		.name = DRV_NAME,
		.of_match_table = virt_reg_test_dt_ids,
	},
};

static int __init virtio_reg_fault_inject_init(void)
{
	int ret;
	tlog(T_INFO, "Fault injection kernel module for virtio regulator initialising\n");

	add_tests();

	ret = platform_driver_register(&virt_reg_test_driver);
	if (ret)
		terr("Error registering platform driver\n");

	tlog(T_INFO, "Driver initialized\n");
	return ret;
}

static void __exit virtio_reg_fault_inject_exit(void)
{
	platform_driver_unregister(&virt_reg_test_driver);
	tlog(T_INFO, "virtio_reg fault injection kernel module Unloaded\n");
	KTF_CLEANUP();
}

module_init(virtio_reg_fault_inject_init);
module_exit(virtio_reg_fault_inject_exit);
MODULE_LICENSE("GPL v2");
