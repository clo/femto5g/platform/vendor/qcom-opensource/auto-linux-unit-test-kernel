/*
* Copyright (c) 2019-2020, The Linux Foundation. All rights reserved.
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 and
* only version 2 as published by the Free Software Foundation.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*/

#include <linux/habmm.h>
#include <linux/module.h>
#include <linux/ktf.h>
#include <linux/ion_kernel.h>
#include <linux/msm_ion.h>
#include <linux/dma-mapping.h>
#include <linux/dma-contiguous.h>
#include <linux/dma-buf.h>

KTF_INIT();

TEST(hab_handler, Test_habmm_unexport)
{
	int32_t handle;
	int32_t open_r;
	int32_t export_r;
	int32_t unexport_r;
	uint32_t export_id1;
	char *addr = NULL;
	struct dma_buf *vhandle = NULL;
	int32_t bufsz = 4096;
	unsigned long err_ion_ptr = 0;
	uint32_t wrong_export_id = 1000;

	vhandle = ion_alloc(bufsz, ION_HEAP(ION_SYSTEM_HEAP_ID), 0);
	if (IS_ERR_OR_NULL((void *)(vhandle))) {
		if (IS_ERR((void *)(vhandle)))
			err_ion_ptr = PTR_ERR((int *)(vhandle));
		tlog(T_INFO, " ION alloc fail err ptr=%ld\n",
		        err_ion_ptr);
		goto err;
	}

	dma_buf_begin_cpu_access((struct dma_buf*)vhandle,
				      DMA_BIDIRECTIONAL);
	addr = dma_buf_vmap((struct dma_buf*)vhandle);
	tlog(T_INFO, "Test_habmm_export begin: habmm_socket_open the return value addr->size=%d \n", addr);

	open_r = habmm_socket_open(&handle, 601, 0, 0);
	tlog(T_INFO, "Test_habmm_unexport begin: habmm_socket_open the return value open_r=%d  handle=0x%x\n", open_r, handle);
	EXPECT_INT_EQ(0, open_r);
	if (open_r < 0)
	{
		goto err;
	}

	tlog(T_INFO, "Test_habmm_unexport  size_bytes is 4096 and flag is 0x00020000\n");
	export_r = habmm_export(handle, vhandle, 4096, &export_id1, HABMM_EXPIMP_FLAGS_DMABUF);
	tlog(T_INFO, "Test_habmm_unexport  size_bytes is 4096 and flag is 0x00020000 the return value export_r=%d export_id=%d \n", export_r, export_id1);
	EXPECT_INT_EQ(0, export_r);

	/* Negative testing */
	/* 1.input invalid export_id */
	tlog(T_INFO, "1.Test_habmm_unexport Negative testing :input invalid export_id  flag=0x00020000\n");
	unexport_r = habmm_unexport(handle, wrong_export_id, 0);
	tlog(T_INFO, "1.Test_habmm_unexport Negative testing the return value unexport_r=%d  flag=0x00020000\n", unexport_r);
	EXPECT_INT_LT(unexport_r, 0);

	/* 2.input invalid handle */
	tlog(T_INFO, "2.Test_habmm_unexport Negative testing :input invalid handle  flag=0x00020000\n");
	unexport_r = habmm_unexport(1, export_id1, 0);
	tlog(T_INFO, "2.Test_habmm_unexport Negative testing the return value unexport_r=%d  flag=0x00020000\n", unexport_r);
	EXPECT_INT_LT(unexport_r, 0);

	/*Positive testing*/
	tlog(T_INFO, "Test_habmm_unexport Positive testing:\n");
	unexport_r = habmm_unexport(handle, export_id1, 0);
	tlog(T_INFO, "Test_habmm_unexport Positive testing flag is 0x00020000 the return value unexport_r=%d export_id1=%d \n", unexport_r, export_id1);
	EXPECT_INT_EQ(0, unexport_r);

	/*free the buf*/
	dma_buf_vunmap((struct dma_buf*)vhandle, addr);
	dma_buf_end_cpu_access((struct dma_buf*)vhandle, DMA_BIDIRECTIONAL);
	dma_buf_put(vhandle);

	habmm_socket_close(handle);
	return;
err:
	terr("Can't open the mmid socket\n");
	return;
}

static void add_tests(void)
{
	ADD_TEST(Test_habmm_unexport);
}


static int __init hab_ut_init(void)
{
	int ret;
	tlog(T_INFO, "Unit test kernel module for hab initialising\n");
	add_tests();
	tlog(T_INFO, "khab test unexport initialized\n");
	return ret;
}

static void __exit hab_ut_exit(void)
{
	tlog(T_INFO, "khab test unexport module unloaded\n");
	KTF_CLEANUP();
}

module_init(hab_ut_init);
module_exit(hab_ut_exit);
MODULE_LICENSE("GPL v2");