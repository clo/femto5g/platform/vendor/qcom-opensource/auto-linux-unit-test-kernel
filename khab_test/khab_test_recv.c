/*
* Copyright (c) 2019-2020, The Linux Foundation. All rights reserved.
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 and
* only version 2 as published by the Free Software Foundation.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*/

#include <linux/habmm.h>
#include <linux/module.h>
#include <linux/ktf.h>

KTF_INIT();

TEST(hab_handler, Test_habmm_socket_recv)
{
	int32_t handle;
	int32_t open_r;
	int32_t recv_r;
	char size_10[10] = "abcdefghi";
	char size_5[5] = "abcd";
	int32_t right_size = 10;
	int32_t wrong_size = 5;

	open_r = habmm_socket_open(&handle, 601, 0, 0);
	tlog(T_INFO, "Test_habmm_socket_recv begin: habmm_socket_open the return value open_r=%d  handle=0x%x\n", open_r, handle);
	EXPECT_INT_EQ(0, open_r);
	if (open_r < 0)
	{
		goto err;
	}

	/* Positive testing */
	tlog(T_INFO, "Test_habmm_socket_recv Positive testing : size_bytes is 10 and flag is 0\n");
	recv_r = habmm_socket_recv(handle, size_10, &right_size, UINT_MAX, 0);
	tlog(T_INFO, "Test_habmm_socket_recv Positive testing size_bytes is 10 and flag is 0 the return value recv_r=%d  right_size：%d\n", recv_r, right_size);
	EXPECT_INT_EQ(0, recv_r);

	recv_r = -1;
	tlog(T_INFO, "Test_habmm_socket_recv Positive testing : size_bytes is 10 and flag is 1\n");

	while(1){
		recv_r = habmm_socket_recv(handle, &size_10, &right_size, UINT_MAX, HABMM_SOCKET_RECV_FLAGS_NON_BLOCKING);
			if(recv_r == 0)
			break;

	}

	tlog(T_INFO, "Test_habmm_socket_recv Positive testing size_bytes is 10 and flag is 1 the return value recv_r=%d  \n", recv_r);
	EXPECT_INT_EQ(0, recv_r);

	/* Negative testing */
	/* 1.Allocated bytes less than actual bytes */
	tlog(T_INFO, "1.habmm_socket_recv Negative testing : Allocated bytes less than actual bytes  flag is 0\n");
	recv_r = habmm_socket_recv(handle, size_5, &wrong_size, UINT_MAX, 0);
	tlog(T_INFO, "1.habmm_socket_recv Negative testing the return value recv_r=%d   flag is 0\n", recv_r);
	EXPECT_INT_LT(recv_r, 0);

	recv_r = 2;
	tlog(T_INFO, "1.habmm_socket_recv Negative testing : Allocated bytes less than actual bytes  flag is 1\n");
	while(1){
		recv_r=habmm_socket_recv(handle,size_5,&wrong_size,UINT_MAX,HABMM_SOCKET_RECV_FLAGS_NON_BLOCKING);
			if(recv_r < 0)
			break;

	}
	tlog(T_INFO, "1.habmm_socket_recv Negative testing the return value recv_r=%d   flag is 1\n", recv_r);
	EXPECT_INT_LT(recv_r, 0);

	/* 2.dst_buff is null */
	tlog(T_INFO, "2.habmm_socket_recv Negative testing : dst_buff is null  flag is 0\n");
	recv_r = habmm_socket_recv(handle, NULL, &right_size, UINT_MAX, 0);
	tlog(T_INFO, "2.habmm_socket_recv Negative testing the return value recv_r=%d  flag is 0 \n", recv_r);
	EXPECT_INT_LT(recv_r, 0);

	recv_r=2;
	tlog(T_INFO, "2.habmm_socket_recv Negative testing : dst_buff is null  flag is 1\n");
	recv_r = habmm_socket_recv(handle, NULL, &right_size, UINT_MAX, HABMM_SOCKET_RECV_FLAGS_NON_BLOCKING);
	tlog(T_INFO, "2.habmm_socket_recv Negative testing the return value recv_r=%d   flag is 1\n", recv_r);
	EXPECT_INT_LT(recv_r, 0);

	/* 3.input invalid handle */
	tlog(T_INFO, "3.habmm_socket_recv Negative testing : input invalid handle flag is 0\n");
	recv_r = habmm_socket_recv(1, size_10, &right_size, UINT_MAX, 0);
	tlog(T_INFO, "3.habmm_socket_recv Negative testing the return value recv_r=%d flag is 0 \n", recv_r);
	EXPECT_INT_LT(recv_r, 0);

	tlog(T_INFO, "3.habmm_socket_recv Negative testing : input invalid handle flag is 1\n");
	recv_r = habmm_socket_recv(1, size_10, &right_size, UINT_MAX, HABMM_SOCKET_RECV_FLAGS_NON_BLOCKING);
	tlog(T_INFO, "3.habmm_socket_recv Negative testing the return value recv_r=%d flag is 1 \n", recv_r);
	EXPECT_INT_LT(recv_r, 0);

	/* 4.input size_bytes as NULL */
	tlog(T_INFO, "3.habmm_socket_recv Negative testing : input size_bytes as NULL flag is 0\n");
	recv_r = habmm_socket_recv(handle, size_10, NULL, UINT_MAX, 0);
	tlog(T_INFO, "3.habmm_socket_recv Negative testing the return value recv_r=%d flag is 0 \n", recv_r);
	EXPECT_INT_LT(recv_r, 0);

	tlog(T_INFO, "3.habmm_socket_recv Negative testing : input size_bytes as NULL flag is 1\n");
	recv_r = habmm_socket_recv(handle, size_10, NULL, UINT_MAX, HABMM_SOCKET_RECV_FLAGS_NON_BLOCKING);
	tlog(T_INFO, "3.habmm_socket_recv Negative testing the return value recv_r=%d flag is 1 \n", recv_r);
	EXPECT_INT_LT(recv_r, 0);

	habmm_socket_close(handle);
	return;
err:
	terr("Can't open the mmid socket\n");
	return;
}


static void add_tests(void)
{
	ADD_TEST(Test_habmm_socket_recv);
}


static int __init hab_ut_init(void)
{
	int ret;
	tlog(T_INFO, "Unit test kernel module for hab initialising\n");
	add_tests();
	tlog(T_INFO, "khab test recv initialized\n");
	return ret;
}

static void __exit hab_ut_exit(void)
{
	tlog(T_INFO, "khab test recv  module unloaded\n");
	KTF_CLEANUP();
}

module_init(hab_ut_init);
module_exit(hab_ut_exit);
MODULE_LICENSE("GPL v2");