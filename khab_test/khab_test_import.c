/*
* Copyright (c) 2019-2020, The Linux Foundation. All rights reserved.
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 and
* only version 2 as published by the Free Software Foundation.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*/

#include <linux/habmm.h>
#include <linux/module.h>
#include <linux/ktf.h>
#include <linux/dma-mapping.h>
#include <linux/dma-contiguous.h>
#include <linux/dma-buf.h>

KTF_INIT();


TEST(hab_handler, Test_habmm_import)
{
	int32_t handle;
	int32_t open_r;
	int32_t recv_r;
	int32_t import_r;
	char export_id1;
	char *addr = NULL;
	uint32_t wrong_export_id = 1000;
	uint32_t size_export_id = 1;
	char *size_10 = NULL;
	char *size_page = NULL;
	int32_t unimport_r;

	open_r = habmm_socket_open(&handle, 601, 0, 0);
	tlog(T_INFO, "Test_habmm_import begin: habmm_socket_open the return value open_r=%d  handle=0x%x\n", open_r, handle);
	EXPECT_INT_EQ(0, open_r);
	if (open_r < 0)
	{
		goto err;
	}

	recv_r = habmm_socket_recv(handle, &export_id1, &size_export_id, 0, 0);
	tlog(T_INFO, "Test_habmm_import : habmm_socket_recv the return value recv_r=%d  export_id1=%d\n", recv_r, export_id1);
	EXPECT_INT_EQ(0, recv_r);

	/* Positive testing */
	tlog(T_INFO, "Test_habmm_import Positive testing : size_bytes is 4096 and flag is 0x00000000\n");
	import_r = habmm_import(handle, (void **)&size_page, 4096, export_id1, 0x00000000);
	tlog(T_INFO, "Test_habmm_import Positive testing size_bytes is 4096 and flag is 0x00000000 the return value import_r=%d export_id1=%d \n", import_r, export_id1);
	EXPECT_INT_EQ(0, import_r);

	addr = (char *)dma_buf_vmap((struct dma_buf*)size_page);
	if (!addr)
	{
		tlog(T_INFO, "Test_habmm_import begin: the return value error!!! \n");
		habmm_unimport(handle, export_id1, (void *)size_page, 0x00000000);
		habmm_socket_close(handle);
		return;
	}

	tlog(T_INFO, "Test_habmm_import begin: the return value *addr=%d \n", *addr);
	*addr=3;
	tlog(T_INFO, "Test_habmm_import begin: dma_buf_vmap the *addr=%d \n", *addr);

	/* unimport the space */
	unimport_r = habmm_unimport(handle, export_id1, (void *)size_page, 0x00000000);
	tlog(T_INFO, "unimport the space  the return value unimport_r=%d export_id1=%d flag=0x00000000\n", unimport_r, export_id1);
	EXPECT_INT_EQ(0, unimport_r);

	/* Negative testing */
	/* 1.input invalid  size_bytes */
	tlog(T_INFO, "1.habmm_import Negative testing :input invalid  size_bytes  flag=0x00000000\n");
	import_r = habmm_import(handle, (void **)&size_10, 10, export_id1, 0x00000000);
	tlog(T_INFO, "1.habmm_import Negative testing the return value import_r=%d  flag=0x00000000\n", import_r);
	EXPECT_INT_LT(import_r, 0);

	/* 2.input invalid  export_id */
	tlog(T_INFO, "2.habmm_import Negative testing :input invalid  export_id  flag=0x00000000\n");
	import_r = habmm_import(handle, (void **)&size_page, 4096, wrong_export_id, 0x00000000);
	tlog(T_INFO, "2.habmm_import Negative testing the return value import_r=%d  flag=0x00000000\n", import_r);
	EXPECT_INT_LT(import_r, 0);

	/* 3.input invalid handle */
	tlog(T_INFO, "3.habmm_import Negative testing :input invalid handle  flag=0x00000000\n");
	import_r = habmm_import(1, (void **)&size_page, 4096, export_id1, 0x00000000);
	tlog(T_INFO, "3.habmm_import Negative testing the return value import_r=%d  flag=0x00000000\n", import_r);
	EXPECT_INT_LT(import_r, 0);

	/* 4.buff_shared is null or invalid address */
	tlog(T_INFO, "4.habmm_import Negative testing :buff_shared is null  flag=0x00000000\n");
	import_r = habmm_import(handle, NULL, 4096, export_id1, 0x00000000);
	tlog(T_INFO, "4.habmm_import Negative testing the return value import_r=%d  flag=0x00000000\n", import_r);
	EXPECT_INT_LT(import_r, 0);

	/* free the buf */
	dma_buf_vunmap((struct dma_buf*)size_page, addr);

	habmm_socket_close(handle);
	return;
err:
	terr("Can't open the mmid socket\n");
	return;
}

static void add_tests(void)
{
	ADD_TEST(Test_habmm_import);
}

static int __init hab_ut_init(void)
{
	int ret;
	tlog(T_INFO, "Unit test kernel module for hab initialising\n");
	add_tests();
	tlog(T_INFO, "khab test import initialized\n");
	return ret;
}

static void __exit hab_ut_exit(void)
{
	tlog(T_INFO, "khab test import module unloaded\n");
	KTF_CLEANUP();
}

module_init(hab_ut_init);
module_exit(hab_ut_exit);
MODULE_LICENSE("GPL v2");