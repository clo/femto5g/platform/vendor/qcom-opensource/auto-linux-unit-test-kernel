/*
* Copyright (c) 2019-2020, The Linux Foundation. All rights reserved.
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 and
* only version 2 as published by the Free Software Foundation.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*/

#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/kprobes.h>
#include <asm/ptrace.h>

#define MAX_PROBES 6
static char *symbol[MAX_PROBES];
static int symbol_num;
static int offset[MAX_PROBES];
module_param_array(symbol, charp, &symbol_num, 0);
module_param_array(offset, int, NULL, 0);

struct probe_point {
	char *api;
	int offset;
	int reg;
	int value;
};

static const struct probe_point probe_lists[] = {
	{ "hab_vchan_open", 0x148, 0, 0 },
	{ "hab_mem_export", 0x38, 22, 0 },
	{ "hab_mem_import", 0x34, 22, 0 },
	{ "hab_mem_unimport", 0x38, 22,0 },
	{ "hab_mem_unexport", 0x30, 22, 0 },
};
/*
 * insmod  khab_unittest_kprobe.ko   symbol=hab_vchan_open     offset=0x148
 * insmod  khab_unittest_kprobe.ko   symbol=hab_mem_export     offset=0x38
 * insmod  khab_unittest_kprobe.ko   symbol=hab_mem_import     offset=0x34
 * insmod  khab_unittest_kprobe.ko   symbol=hab_mem_unimport   offset=0x38
 * insmod  khab_unittest_kprobe.ko   symbol=hab_mem_unexport   offset=0x30
 */
static int kp_index, kp2_index;
static int kp_value, kp2_value;

/* For each probe you need to allocate a kprobe structure */
static struct kprobe kp;
static struct kprobe kp2;

static struct kprobe *kps[2] = {&kp, &kp2};

/* kprobe pre_handler: called just before the probed instruction is executed */
static int kp_pre_handler(struct kprobe *p, struct pt_regs *regs)
{
	printk(KERN_INFO "%s: p->addr=0x%pK, x[%d]=0x%lx\n", __func__,p->addr, kp_index, regs->user_regs.regs[kp_index]);
	regs->user_regs.regs[kp_index] = kp_value;
	/* A dump_stack() here will give a stack backtrace */
	return 0;
}

static int kp2_pre_handler(struct kprobe *p, struct pt_regs *regs)
{
	printk(KERN_INFO "%s: p->addr=0x%pK, x[%d]=0x%lx\n", __func__,p->addr, kp2_index, regs->user_regs.regs[kp2_index]);
	regs->user_regs.regs[kp2_index] = kp2_value;
	/* A dump_stack() here will give a stack backtrace */
	return 0;
}

/* kprobe post_handler: called after the probed instruction is executed */
static void kp_post_handler(struct kprobe *p, struct pt_regs *regs,
	unsigned long flags)
{
	printk(KERN_INFO "%s: p->addr=0x%pK, x[%d]=0x%lx, flags=%ld\n",__func__, p->addr, kp_index, regs->user_regs.regs[kp_index], flags);
	regs->user_regs.regs[kp_index] = kp_value;
	printk(KERN_INFO "%s: x[%d]=0x%lx", __func__, kp_index, regs->user_regs.regs[kp_index]);
}

static void kp2_post_handler(struct kprobe *p, struct pt_regs *regs,
	unsigned long flags)
{
	printk(KERN_INFO "%s: p->addr=0x%pK, x[%d]=0x%lx, flags=%ld\n",__func__, p->addr, kp2_index, regs->user_regs.regs[kp2_index], flags);
	regs->user_regs.regs[kp2_index] = kp2_value;
	printk(KERN_INFO "%s: x[%d]=0x%lx", __func__, kp2_index, regs->user_regs.regs[kp2_index]);
}

/*
 * fault_handler: this is called if an exception is generated for any
 * instruction within the pre- or post-handler, or when Kprobes
 * single-steps the probed instruction.
 */
static int kp_fault_handler(struct kprobe *p, struct pt_regs *regs, int trapnr)
{
	printk(KERN_INFO "fault_handler: p->addr = 0x%p, trap #%d\n",p->addr, trapnr);
	/* Return 0 because we don't handle the fault. */
	return 0;
}

static int __init kprobe_init(void)
{
	int i, j, ret, list_len;
	kprobe_opcode_t *symbol_addr;

	list_len = sizeof(probe_lists)/sizeof(struct probe_point);

	printk(KERN_INFO "symbol numbers = %d, list number = %d\n", symbol_num, list_len);
	for (i=0; i<symbol_num; i++) {
	for (j=0; j<list_len; j++) {
	if (!strcmp(symbol[i], probe_lists[j].api) && (offset[i] == probe_lists[j].offset)) {
		if (i == 0) {
				kp.symbol_name = probe_lists[j].api;
				kp.offset = probe_lists[j].offset;
				kp.pre_handler = kp_pre_handler;
				kp.post_handler = kp_post_handler;
				kp.fault_handler = kp_fault_handler;
				kp_index = probe_lists[j].reg;
				kp_value = probe_lists[j].value;
				}
			if (i == 1) {
				kp2.symbol_name = probe_lists[j].api;
				kp2.offset = probe_lists[j].offset;
				kp2.pre_handler = kp2_pre_handler;
				kp2.post_handler = kp2_post_handler;
				kp2.fault_handler = kp_fault_handler;
				kp2_index = probe_lists[j].reg;
				kp2_value = probe_lists[j].value;
				}
			}
		}
	}

	symbol_addr = (kprobe_opcode_t *)kallsyms_lookup_name(kp.symbol_name);
	if (!symbol_addr) {
		printk(KERN_INFO "Could not find address of %s\n", kp.symbol_name);
		return -1;
		}
	printk(KERN_INFO "kp1.symbol_addr = 0x%lx\n", symbol_addr);

	if (symbol_num == 1) {
		ret = register_kprobe(&kp);
		if (ret < 0) {
			printk(KERN_INFO "register_kprobe failed, returned %d\n", ret);
			return ret;
		}
		printk(KERN_INFO "Planted kprobe1: %s+%pK at %pK\n", kp.symbol_name, (kprobe_opcode_t *)kp.offset, (kprobe_opcode_t *)kp.addr);
	}
	else if (symbol_num == 2) {
		ret = register_kprobes(kps, 2);
		if (ret < 0) {
			printk(KERN_INFO "register_kprobe failed, returned %d\n", ret);
			return ret;
		}
		printk(KERN_INFO "Planted kprobe1: %s+%pK at %pK; kprobe2: %s+%pK at %pK\n", kp.symbol_name, (kprobe_opcode_t *)kp.offset,
		(kprobe_opcode_t *)kp.addr, kp2.symbol_name, (kprobe_opcode_t *)kp2.offset, (kprobe_opcode_t *)kp2.addr);
	}

	return 0;
}

static void __exit kprobe_exit(void)
{
	if (symbol_num == 1) {
		unregister_kprobe(&kp);
		printk(KERN_INFO "kprobe at %pK unregistered\n", kp.addr);
	}
	else {
		unregister_kprobes(kps, 2);
		printk(KERN_INFO "kprobe1 at %pK and kprobe2 at %pK unregistered\n", kp.addr, kp2.addr);
	}
}

module_init(kprobe_init);
module_exit(kprobe_exit);
MODULE_LICENSE("GPL v2");