/*
* Copyright (c) 2019-2020, The Linux Foundation. All rights reserved.
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 and
* only version 2 as published by the Free Software Foundation.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*/

#include <linux/habmm.h>
#include <linux/module.h>
#include <linux/ktf.h>

KTF_INIT();

TEST(hab_handler, Test_habmm_socket_query)
{
	int32_t handle;
	int32_t open_r;
	int32_t query_r;
	struct hab_socket_info info;

	tlog(T_INFO, "Test_habmm_socket_query begin\n");
	open_r = habmm_socket_open(&handle, 601, 0, 0);
	tlog(T_INFO, "Test_habmm_socket_query  the habmm_socket_open return value open_r=%d  handle=0x%x\n", open_r, handle);
	EXPECT_INT_EQ(0, open_r);
	if (open_r < 0)
	{
		goto err;
	}

	/* Positive testing */
	tlog(T_INFO, "Test_habmm_socket_query Positive testing:\n");
	query_r = habmm_socket_query(handle, &info, 0);
	tlog(T_INFO, "Test_habmm_socket_query Positive testing  the return value query_r=%d \n", query_r);
	EXPECT_INT_EQ(0, query_r);
	tlog(T_INFO,"Test_habmm_socket_query hab_socket_info ： vmid local %d remote %d, vmname local %s remote %s\n",
		info.vmid_local, info.vmid_remote, info.vmname_local, info.vmname_remote);

	/* Negative testing */
	/* 1.input invalidate handle */
	tlog(T_INFO, "1.Test_habmm_socket_query Negative testing :input invalidate handle  \n");
	query_r = habmm_socket_query(1, &info, 0);
	tlog(T_INFO, "1.Test_habmm_socket_query Negative testing the return value query_r=%d  \n", query_r);
	EXPECT_INT_LT(query_r, 0);

	/* 2.input info is null */
	tlog(T_INFO, "2.Test_habmm_socket_query Negative testing :input info is null  \n");
	query_r = habmm_socket_query(handle, NULL, 0);
	tlog(T_INFO, "2.Test_habmm_socket_query Negative testing the return value query_r=%d  \n", query_r);
	EXPECT_INT_LT(query_r, 0);

	habmm_socket_close(handle);
	return;
err:
	terr("Can't open the mmid socket\n");
	return;
}

static void add_tests(void)
{
	ADD_TEST(Test_habmm_socket_query);
}

static int __init hab_ut_init(void)
{
	int ret;
	tlog(T_INFO, "Unit test kernel module for hab initialising\n");
	add_tests();
	tlog(T_INFO, "khab test query initialized\n");
	return ret;
}

static void __exit hab_ut_exit(void)
{
	tlog(T_INFO, "khab test query module unloaded\n");
	KTF_CLEANUP();
}

module_init(hab_ut_init);
module_exit(hab_ut_exit);
MODULE_LICENSE("GPL v2");