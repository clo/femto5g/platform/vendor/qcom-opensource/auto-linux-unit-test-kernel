/*
* Copyright (c) 2019-2020, The Linux Foundation. All rights reserved.
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 and
* only version 2 as published by the Free Software Foundation.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*/

#include <linux/habmm.h>
#include <linux/module.h>
#include <linux/ktf.h>

KTF_INIT();

TEST(hab_handler, Test_habmm_socket_open)
{
	int32_t handle;
	int32_t open_r;

	/* Test_habmm_socket_open  testing */
	tlog(T_INFO, "fault injection Test_habmm_socket_open  testing begin: valid mmid is 601\n");
	open_r = habmm_socket_open(&handle, 601, 0, 0);
	tlog(T_INFO, "fault injection habmm_socket_open fault injection  testing the return value open_r=%d  handle=0x%x\n", open_r, handle);
	EXPECT_INT_LT(open_r, 0);

	if(open_r == 0)
	{
		habmm_socket_close(handle);
	}
	return;
err:
	terr("fault injection for Test_habmm_socket_open failed\n");
	return;
}

static void add_tests(void)
{
	ADD_TEST(Test_habmm_socket_open);
}

static int __init hab_ut_init(void)
{
	int ret;
	tlog(T_INFO, "fault injection test kernel module for hab open initialising\n");
	add_tests();
	tlog(T_INFO, "fault injection khab test open initialized\n");
	return ret;
}

static void __exit hab_ut_exit(void)
{
	tlog(T_INFO, "fault injection khab test open  module unloaded\n");
	KTF_CLEANUP();
}

module_init(hab_ut_init);
module_exit(hab_ut_exit);
MODULE_LICENSE("GPL v2");