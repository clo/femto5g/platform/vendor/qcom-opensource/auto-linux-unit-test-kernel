/*
* Copyright (c) 2019-2020, The Linux Foundation. All rights reserved.
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 and
* only version 2 as published by the Free Software Foundation.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*/

#include <linux/habmm.h>
#include <linux/module.h>
#include <linux/ktf.h>

KTF_INIT();

TEST(hab_handler, Test_habmm_import)
{
	int32_t handle;
	int32_t open_r;
	int32_t recv_r;
	int32_t import_r;
	char export_id;
	uint32_t size_export_id = 1;
	char *size_page = NULL;

	open_r = habmm_socket_open(&handle, 601, 0, 0);
	tlog(T_INFO, "fault injection Test_habmm_import begin: habmm_socket_open the return value open_r=%d  handle=0x%x\n", open_r, handle);
	EXPECT_INT_EQ(0, open_r);
	if (open_r < 0)
	{
		goto err;
	}

	recv_r = habmm_socket_recv(handle, &export_id, &size_export_id, 0, 0);
	tlog(T_INFO, "fault injection  Test_habmm_import : habmm_socket_recv the return value recv_r=%d  export_id=%d\n", recv_r, export_id);
	EXPECT_INT_EQ(0, recv_r);

	/* fault injection Test_habmm_import  testing */
	tlog(T_INFO, "fault injection Test_habmm_import  testing : size_bytes is 4096 and flag is 0x00000001\n");
	import_r = habmm_import(handle, (void **)&size_page, 4096, export_id, 0x00000001);
	tlog(T_INFO, "fault injection Test_habmm_import  testing size_bytes is 4096 and flag is 0x00000001 the return value import_r=%d export_id=%d \n", import_r, export_id);
	EXPECT_INT_LT(import_r, 0);

	habmm_socket_close(handle);
	return;

err:
	terr("Can't open the mmid socket\n");
	return;
}

static void add_tests(void)
{
	ADD_TEST(Test_habmm_import);
}

static int __init hab_ut_init(void)
{
	int ret;
	tlog(T_INFO, "fault injection khab test kernel module for hab initialising\n");
	add_tests();
	tlog(T_INFO, "fault injection khab test import initialized\n");
	return ret;
}

static void __exit hab_ut_exit(void)
{
	tlog(T_INFO, "fault injection khab test import module unloaded\n");
	KTF_CLEANUP();
}

module_init(hab_ut_init);
module_exit(hab_ut_exit);
MODULE_LICENSE("GPL v2");